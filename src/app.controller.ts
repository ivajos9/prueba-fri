import {Controller, Get, Query} from '@nestjs/common';
import {AppService} from './app.service';
import {ApiUtils} from "./utils/api-utils";

@Controller('/api')
export class AppController {
    constructor(private readonly appService: AppService) {
    }

    @Get('/search')
    searchOnServices(@Query() query) {
        let results = null;

        results = this.appService.searchOnservices(query);
        if (results) {
            return results;
        } else {
            ApiUtils.throwHttpException(400, "No results found");
        }
    }
}
