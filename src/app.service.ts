import {HttpService, Injectable, Query} from '@nestjs/common';
import {XmlRequest} from "./xml-service/XmlRequest";
import {ApiUtils} from "./utils/api-utils";
import * as soapRequest from 'easy-soap-request';
import {ConvertXmlService} from "./xml-service/ConvertXmlService";
import {ServiceResult} from "./bo/ServiceResult";

@Injectable()
export class AppService {

    constructor(
        private httpService: HttpService,
    ) {
    }

    async searchOnservices(@Query() query) {
        let resultList = [];
        // Se obtienen respuestas y se consolidan para entregar en una sola respuesta
        let persons = await this.searchPersons(query.keyword);
        if (persons && persons.length > 0) {
            resultList = this.getDataToShow(resultList, "crcind", persons);
        }
        let shows = await this.searchShows(query.keyword);
        if (shows && shows.length > 0) {
            resultList = this.getDataToShow(resultList, "tvmaze", shows);
        }
        let music = await this.searchMusic(query.keyword);
        if (music && music.results.length > 0) {
            resultList = this.getDataToShow(resultList, "itunes", music.results);
        }


        return resultList;
    }


    async searchPersons(keyword: string) {
        // Metodo de envio de peticion SOAP
        const xmlRequest = XmlRequest(keyword);

        const requestHeaders = {
            'user-agent': 'sampleTest',
            'Content-Type': 'text/xml;charset=UTF-8',
            'soapAction': `http://tempuri.org/SOAP.Demo.GetListByName`,
        };
        try {

            const {response} = await soapRequest({
                url: ApiUtils.ENDPOINTS.CRCRINDURI,
                headers: requestHeaders,
                xml: xmlRequest,
                timeout: 100000,
            });
            const {body} = response;
            console.log(body);
            return ConvertXmlService.parseRen(body).Envelope.Body.GetListByNameResponse.GetListByNameResult.PersonIdentification;
        } catch (ex) {
            return null;
        }

    }

    async searchShows(keyword) {
        // Metodo de envio a API rest de shows
        try {
            let showResults = await this.httpService.get(`${ApiUtils.ENDPOINTS.TVMAZE}?q=${keyword}`).toPromise();
            return showResults.data;
        } catch (ex) {
            return null;
        }
    }

    async searchMusic(keyword) {
        // Metodo de envio a API rest de musica
        try {
            let musicResult = await this.httpService.get(`${ApiUtils.ENDPOINTS.ITUNESSEAPI}?term=${keyword}&media=music`).toPromise();
            console.log(`${ApiUtils.ENDPOINTS.ITUNESSEAPI}?term=${keyword}&media=music`);
            return musicResult.data;
        } catch (ex) {
            return null;
        }

    }

    getDataToShow(list: any[], serviceName, data: any[]) {
        for (let value of data) {
            let val = new ServiceResult();
            val.service = serviceName;
            val.result = value
            list.push(val);
        }
        return list;
    }
}
