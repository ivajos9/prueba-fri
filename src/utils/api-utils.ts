import {HttpException} from '@nestjs/common/exceptions';
import {HttpStatus} from '@nestjs/common/enums';

export class ApiUtils {

    static DEV_VARS = {
        CONTRACT_EMAIL: null,
    };
    static ENDPOINTS = {
        CRCRINDURI: 'http://www.crcind.com/csp/samples/SOAP.Demo.cls',
        ITUNESSEAPI: ' https://itunes.apple.com/search',
        TVMAZE: 'http://api.tvmaze.com/search/shows'

    };

    static throwHttpException(httpStatusCode: any, message?: string) {
        switch (httpStatusCode) {
            case 400:
                throw new HttpException(message, HttpStatus.BAD_REQUEST);
            case 401:
                throw new HttpException(message, HttpStatus.UNAUTHORIZED);
            case 403:
                throw new HttpException(message, HttpStatus.FORBIDDEN);
            case 404:
                throw new HttpException(message, HttpStatus.NOT_FOUND);
            case 409:
                throw new HttpException(message, HttpStatus.CONFLICT);
            case 415:
                throw new HttpException(message, HttpStatus.UNSUPPORTED_MEDIA_TYPE);
            case 422:
                throw new HttpException(message, HttpStatus.UNPROCESSABLE_ENTITY);
            case 500:
                throw new HttpException(message, HttpStatus.INTERNAL_SERVER_ERROR);
            default:
                throw new HttpException(message, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
