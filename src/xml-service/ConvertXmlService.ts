import * as convert from 'xml-js';

export class ConvertXmlService {

    constructor() {
    }

    static parse(xml: any): any {
        const options = {
            compact: true,
            trim: true,
            allwaysArray: true,
            ignoreDeclaration: true,
            ignoreInstruction: true,
            ignoreAttributes: true,
            ignoreComment: true,
            ignoreCdata: true,
            ignoreDoctype: true,
            textFn: this.removeJsonTextAttribute,
            elementNameFn: this.removeKeys,
        };
        return convert.xml2js(xml, options);
    }

    static removeJsonTextAttribute(value, parentElement) {
        try {
            const parentOfParent = parentElement._parent;
            const pOpKeys = Object.keys(parentElement._parent);
            const keyNo = pOpKeys.length;
            const keyName = pOpKeys[keyNo - 1];
            const arrOfKey = parentElement._parent[keyName];
            const arrOfKeyLen = arrOfKey.length;
            if (arrOfKeyLen > 0) {
                const arr = arrOfKey;
                const arrIndex = arrOfKey.length - 1;
                arr[arrIndex] = value;
            } else {
                parentElement._parent[keyName] = value;
            }
        } catch (e) {
            console.log(e);
        }
    }

    static removeKeys(val) {
        let keys = val;

        if (val.substring(0, 2) === 'S:') {
            keys = val.replace('S:', '');
        }

        if (val.substring(0, 4) === 'ns1:') {
            keys = val.replace('ns1:', '');
        }

        if (val.substring(0, 4) === 'ns0:') {
            keys = val.replace('ns0:', '');
        }

        if (val.substring(0, 4) === 'ns2:') {
            keys = val.replace('ns2:', '');
        }

        return keys;
    }

    // Rempvaciones
    static parseRen(xml: any): any {
        const options = {
            compact: true,
            trim: true,
            allwaysArray: true,
            ignoreDeclaration: true,
            ignoreInstruction: true,
            ignoreAttributes: true,
            ignoreComment: true,
            ignoreCdata: true,
            ignoreDoctype: true,
            textFn: this.removeJsonTextAttributeRen,
            elementNameFn: this.removeKeysRen,
        };
        return convert.xml2js(xml, options);
    }

    static removeJsonTextAttributeRen(value, parentElement) {
        try {
            const parentOfParent = parentElement._parent;
            const pOpKeys = Object.keys(parentElement._parent);
            const keyNo = pOpKeys.length;
            const keyName = pOpKeys[keyNo - 1];
            const arrOfKey = parentElement._parent[keyName];
            const arrOfKeyLen = arrOfKey.length;
            if (arrOfKeyLen > 0) {
                const arr = arrOfKey;
                const arrIndex = arrOfKey.length - 1;
                arr[arrIndex] = value;
            } else {
                parentElement._parent[keyName] = value;
            }
        } catch (e) {
            console.log(e);
        }
    }

    static removeKeysRen(val) {
        let keys = val;

        if (val.substring(0, 9) === 'SOAP-ENV:') {
            keys = val.replace('SOAP-ENV:', '');
        }

        return keys;
    }

}
