export const XmlRequest = (name) => {
    return `
    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org">
   <soapenv:Header/>
   <soapenv:Body>
      <tem:GetListByName>
         <tem:name>${name}</tem:name>
      </tem:GetListByName>
   </soapenv:Body>
</soapenv:Envelope>     
`;
};
