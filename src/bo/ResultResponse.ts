import {ServiceResult} from "./ServiceResult";

export class ResultResponse {
    statusCode: string;
    message: string;
    results: ServiceResult[];
}
